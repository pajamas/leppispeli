import pygame
import random

window_width = 1000
window_height = 800
window = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Leppispeli")

fps = 60
clock = pygame.time.Clock()

white = (255, 255, 255)

ground_color = (171, 237, 130)
ground_start_y = 550
ground = pygame.Rect(0, ground_start_y, window_width, window_height - ground_start_y)

leppis_image = pygame.image.load("leppis.png")
leppis_x = 350
leppis_y = ground_start_y - leppis_image.get_height() + 12
leppis_speed = 7

rock_image = pygame.image.load("rock.png")
coin_image = pygame.image.load("coin.png")

def random_x():
    return random.randint(0, window_width - rock_image.get_width())

rocks = [ [random_x(), -50] ]
coins = [ [random_x(), -50] ]

gravity = 6

running = True

while running:
    clock.tick(fps)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    rand_num = random.randint(0,100)
    if rand_num < 5:
        rocks.append([random_x(), -50])
    elif rand_num > 95:
        coins.append([random_x(), -50])

    pressed_keys = pygame.key.get_pressed()

    if pressed_keys[pygame.K_LEFT] and leppis_x - leppis_speed > 0:
        leppis_x = leppis_x - leppis_speed
    right_boundary = window_width - leppis_image.get_width()
    if pressed_keys[pygame.K_RIGHT] and leppis_x + leppis_speed < right_boundary:
        leppis_x = leppis_x + leppis_speed
    
    for rock_coords in rocks:
        rock_coords[1] += gravity
        if rock_coords[1] > ground_start_y - rock_image.get_height():
            rocks.remove(rock_coords)
    for coin_coords in coins:
        coin_coords[1] += gravity
        if coin_coords[1] > ground_start_y - coin_image.get_height():
            coins.remove(coin_coords)
    
    window.fill(white)
    pygame.draw.rect(window, ground_color, ground)
    window.blit(leppis_image, (leppis_x, leppis_y))
    for rock_coords in rocks:
        window.blit(rock_image, rock_coords)
    for coin_coords in coins:
        window.blit(coin_image, coin_coords)
    pygame.display.update()
